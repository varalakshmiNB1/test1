package com.qaagility;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingListener; // Unnecessary import which can be removed to clear PMD warning
import java.util.Date; // Unnecessary import which can be removed to clear PMD warning

public class HelloWebApp extends HttpServlet {

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {

            PrintWriter out = resp.getWriter();
            
            resp.setContentType("text/html");

            out.println("<html>");
            out.println("<body bgcolor=\"Aqua\">");

            out.println("<h1>Hello World from DevOps Alliance and ATA</h1>");
            resp.getWriter().write("Java Project for CPDOF Session by test varalakshmi june-2020..\n\n\n");

            out.println("</body>");
            out.println("</html>");   
        }


        public int add(int a, int b) {
            int ret = a + b; // Unnecessary declaration which can be removed to clear PMD warning
            return a + b;
        }

	public int sub(int a, int b) {
            return a - b;
        }

}
